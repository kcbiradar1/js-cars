function problem3(inventory) {
    if(Array.isArray(inventory)) {
        let model_name = [];
        for(let car = 0; car < inventory.length; car++) {
            model_name.push(inventory[car].car_model);
        }
        model_name.sort();
        return model_name;
    } else {
        return [];
    }
}

module.exports = problem3