
function problem1(inventory , passid) {
    if(Array.isArray(inventory) && passid > 0) {
        for(let car = 0; car < inventory.length; car++) {
            if(inventory[car].id === passid) {
                return inventory[car];
            }
        }
    } else {
        return [];
    }
}

module.exports = problem1;