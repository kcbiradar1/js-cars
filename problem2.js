
function problem2(inventory) {
    if(Array.isArray(inventory)) {
        let number_of_cars = inventory.length - 1;
        return inventory[number_of_cars];
    } else {
        return [];
    }
}

module.exports = problem2;