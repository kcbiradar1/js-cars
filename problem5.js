const problem4 = require('./problem4');

function problem5(inventory) {
    if(Array.isArray(inventory)) {
        let years = [];
        const years_data = problem4(inventory);
        for(let index = 0; index < years_data.length; index++) {
            if(years_data[index] < 2000) {
                years.push(years_data[index]);
            }
        }
        return years;
    } else {
        return [];
    }
}

module.exports = problem5;