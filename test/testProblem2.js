const inventory = require('../data');

const problem2 = require('../problem2');

const results = problem2(inventory);

console.log(`Last car is a  ${results.car_make} ${results.car_model}`);