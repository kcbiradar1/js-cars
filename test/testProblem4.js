
const inventory = require('../data');

const problem4 = require('../problem4');

const results = problem4(inventory);

console.log(results);

let years = []

for(let index = 0; index < results.length; index++) {
    if(results[index] < 2000) {
        years.push(results[index]);
    }
}

module.exports = years;