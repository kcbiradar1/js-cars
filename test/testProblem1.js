
const inventory = require('../data');

const problem1 = require('../problem1');

const results = problem1(inventory,passid);

console.log(`Car 33 is a ${results.car_year} ${results.car_make} ${results.car_model}`);