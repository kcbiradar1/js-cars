
function problem4(inventory) {
    if(Array.isArray(inventory)) {
        let years = [];
        for(let car = 0; car < inventory.length; car++) {
            years.push(inventory[car].car_year);
        }
        return years;
    } else {
        return [];
    }
}

module.exports = problem4;